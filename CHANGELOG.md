# 0.3.5 - 2021-08-22

* Don't offer snapshot builds of upcoming releases anymore
* Translation impromevements
* Remove [*ASU*](https://github.com/aparcar/asu/) integration for now.
  * The feature wasn't fully integrated therefore it should be removed for now.
